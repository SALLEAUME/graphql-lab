package com.soprasteria.graphqllab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.soprasteria.graphqllab")
public class GraphqllabApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqllabApplication.class, args);
	}

}
